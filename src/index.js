
/* eslint eqeqeq: "off" */
//@flow

import * as React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import {HashRouter, Route, NavLink} from 'react-router-dom';
import {Component} from "react-simplified";
import Ticker from 'react-ticker';
import Image from 'react-bootstrap/Image';

class Article{
    title: string;
    imageURL: string;
    time: string;
    content: string;
    category: number;
    important: boolean;
    constructor(title: string, imageURL: string, time: string, content: string, category: number, important: boolean){
        this.title = title;
        if (imageURL == null){
            this.imageURL = 'http://clipart-library.com/images_k/lawn-care-silhouette/lawn-care-silhouette-21.png';
        }
        this.time = time;
        this.content = content;
        this.category = category;
        this.important = important;
    }
}

let articles = [
    new Article("Tittel1", null, "2019-16-10 11:15", "Some content", 1, true),
    new Article("Tittel2", null, "2019-16-10 11:16", "Some content", 1, true),
    new Article("Tittel3", null, "2019-16-10 11:17", "Some content", 1, true),
    new Article("Tittel4", null, "2019-16-10 11:18", "Some content", 1, true)
];

class Header extends Component{
    render(){
        return(
            <nav className='navbar navbar-expand-sm bg-light navbar-light'>
                <NavLink className='navbar-brand' activeClassName='active' exact to='/'><img src='http://clipart-library.com/images_k/lawn-care-silhouette/lawn-care-silhouette-21.png' alt='koalalogo' style={{width:'50px'}} /></NavLink>
                <div className='navbar-nav w-100'>
                    <NavLink className='nav-link text-info' activeClassName='active' to='/'>HJEM</NavLink>
                    <NavLink className='nav-link text-info' activeClassName='active' to='/'>Kategori 1</NavLink>
                    <NavLink className='nav-link text-info' activeClassName='active' to='/'>Kategori 2</NavLink>
                    <NavLink className='nav-link text-info' activeClassName='active' to='/'>Kategori 3</NavLink>
                </div>
                <div className='navbar-nav w-25'>
                    <NavLink className='nav-link text-info' activeClassName='active' to='/'>Registrer sak</NavLink>
                </div>
            </nav>
        );
    }
}

class LiveFeed extends Component{
    render(){
        return (
            <div className='card'>
                <div className='card-body'>
                    <Ticker>
                        {({ index }) => (
                            <>
                            <div className='card-text'>{articles.map(e => (
                                <NavLink className='text-info feed-content' to='#'>
                                {e.title} &nbsp; {e.time}
                                </NavLink>))}
                            </div>
                            </>
                        )}

                    </Ticker>
                </div>
            </div>
        );
    }
}

class Card extends Component<{title: React.Node, children?: React.Node}>{
    render(){
        return(
          <div className='card text-center article-card'>
            <div className='card-body'>
                <h3 className='card-title'>{this.props.title}</h3>
                <div className='card-img'>{this.props.children}</div>
            </div>
          </div>
        );
    }
}

class ArticleBox extends Component<{title: React.Node, imageURL: React.Node}>{
    render(){
        return(
          <div className='container m-4 article-box'>
            <Card title={this.props.title} children={<Image alt={this.props.title} src={this.props.imageURL} fluid/>}/>
          </div>
        );
    }
}

class ArticleGrid extends Component{
    render(){
        return(
          <div className='container'>
              {articles.filter(e => e.important).reduce( function(accumulator, currentValue, currentIndex, array){
                if (currentIndex % 2 == 0){
                    accumulator.push(currentIndex);
                }
                return accumulator;
              }, []).map((e, index) => ((
                  <div className='row' key={e.title}>
                    <div className='col'><NavLink className='text-dark' to='#'><ArticleBox title={articles[e].title} imageURL={articles[e].imageURL}/></NavLink></div>
                    <div className='col'><NavLink className='text-dark' to='#'><ArticleBox title={articles[e + 1].title} imageURL={articles[e + 1].imageURL}/></NavLink></div>
                  </div>)))}
          </div>

        );
    }
}

const root = document.getElementById('root');
if (root)
    ReactDOM.render(
      <HashRouter>
          <div>
          <Header />
          <Route exact path="/" component={LiveFeed}/>
          <ArticleGrid/>
          </div>
      </HashRouter>,
        root
    );
